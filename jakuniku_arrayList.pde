//当面の目標
//カプセル化する（ゲッター、セッター、private）
public static final int team_red=1000;
public static final int team_green=1000;
public static final int team_blue=1000;

int score;
int itmp=0;
int flag=0;

boolean initialize=true;   
int pattern=1;
int red_num=5;//赤の初期上限値
int green_num=5;//緑の初期上限値
int blue_num=5;//青の初期上限値

PImage red_plus,red_minus,green_plus,green_minus,blue_plus,blue_minus;//画像の入れ物生成
boolean start_flag=false;//スタートするかどうか。false＝偽→開始しない
PFont font = createFont("MS Gothic", 80,true); //フォントの設定


ArrayList<Red> r;//赤の配列みたいなものの生成
ArrayList<Green> g;//緑の配列みたいなものの生成

void setup(){
  size(1080,720);//画面を1080＊720に
  smooth();//画像などをスムージング（綺麗）にしてくれる（？）
  frameRate(30);//フレームレートを30に固定する
  r = new ArrayList<Red>();//rというリストに赤の
  g = new ArrayList<Green>();//
  
  
  red_plus=loadImage("plus_r.png");//赤のプラス読み込み
  red_minus=loadImage("minus_r.png");//赤のマイナス読み込み
  green_plus=loadImage("plus_g.png");//緑のプラス読み込み
  green_minus=loadImage("minus_g.png");//緑のマイナス読み込み
  blue_plus=loadImage("plus_b.png");//青のプラス読み込み
  blue_minus=loadImage("minus_b.png");//青のマイナス読み込み



}

void draw(){
  background(0);
  //スタートフラグが偽→開始しない
  if(start_flag==false){
    draw_title();//タイトル画面描画関数の呼び出し
    title_process();//タイトル画面の処理関数の呼び出し
  }

  //スタートフラグが真→シミュレート開始
  else if(start_flag==true){

    if (initialize==true) {
       
     
      
      for(int i=0;i<red_num;i++){
      //赤の生成
        r.add(new Red(random(1020)+30,random(660)+30,random(10000)/10000.f * (PI*2),1+random(2),1));
      }
      for(int i=0;i<green_num;i++){
        g.add(new Green(random(1020)+30,random(660)+30,random(10000)/10000.f * (PI*2),1+random(2),1));
      }
      initialize=false;
    }
    draw_simulate();//シミュレート画面の描画
    simulate_process();//シミュレート処理関数を呼び出し  

  }
}

//タイトル画面描画
void draw_title(){
  
    image(red_plus,247.5,height*3/5);
    image(red_minus,247.5,height*4/5);
    textSize(30);
    text(red_num,260.5,height*3.5/5+30);
    image(green_plus,495,height*3/5);
    image(green_minus,495,height*4/5);
    text(green_num,507.5,height*3.5/5+30);
    image(blue_plus,742.5,height*3/5);
    image(blue_minus,742.5,height*4/5);
    text(blue_num,754.5,height*3.5/5+30);
    
    

    textAlign(CENTER);
    textFont(font);
    fill(255,255,255);
    text("弱肉強食",width/2,160);
    textSize(30);
    
    text("〜食物連鎖シミュレーター〜",width/2,200);
    textSize(50);
    text("aを押したらシミュレート開始！",width/2,360);
  
    
   
}
//タイトル画面の処理関数
void title_process(){

  if ((keyPressed == true) && (key == 'a')) {
    start_flag=true;
  }
  
  
  //個体数を上下させる
  if(mousePressed == true){
    if(red_num < 99 && mouseX > 247.5 && mouseY > height*3/5 && mouseX < 277.5 && mouseY < height*3/5 + 30 ){
      red_num++;
    }
    if(red_num > 2 && mouseX > 247.5 && mouseY > height*4/5 && mouseX < 277.5 && mouseY < height*4/5 + 30 ){
      red_num--;
    }
    
    if(green_num < 99 && mouseX > 495 && mouseY > height*3/5 && mouseX < 525 && mouseY < height*3/5 + 30 ){
      green_num++;
    }
    if(green_num > 2 && mouseX > 495 && mouseY > height*4/5 && mouseX < 525 && mouseY < height*4/5 + 30 ){
      green_num--;
    }
    if(blue_num < 99 && mouseX > 742.5 && mouseY > height*3/5 && mouseX < 772.5 && mouseY < height*3/5 + 30 ){
      blue_num++;
    }
    if(blue_num > 2 && mouseX > 742.5 && mouseY > height*4/5 && mouseX < 772.5 && mouseY < height*4/5 + 30 ){
      blue_num--;
    }
    
  }

  
}
//シミュレート画面描画関数

void draw_simulate(){
  for(int i = 0 ;i < r.size(); i++){
    r.get(i).display();  

  }
  for(int i = 0 ;i < g.size(); i++){
    g.get(i).display();    
  }
  
}
//シミュレート画面の処理関数
void simulate_process(){
  if(flag==0){
    score=frameCount;
    flag=1;
  }
  
  for(int i = 0;i < green_num;i++){
    
    g.get(i).move();//移動メソッドの呼び出し
    g.get(i).display();//描画メソッドの呼び出し
    g.get(i).hungry--;
    //text(g.get(0).hungry,30,300);
      
    if(g.get(i).hungry < 0){
      g.get(i).die_of_hunger(i);
      green_num--;
    }
  }
  for(int i = 0;i < red_num;i++){
    
    r.get(i).move();//移動メソッドの呼び出し
    r.get(i).display();//描画メソッドの呼び出し
    r.get(i).hungry--;
      
    if(r.get(i).hungry < 0){
      r.get(i).die_of_hunger(i);
      red_num--;
    }
  }
  
  //子孫繁栄関数
  if(frameCount % 10 == 0){
    bear();
  }
  eat('r');
  //eat(b);
  
  //終了判定
  if(red_num==0 && green_num==0){
    if(itmp==0){
      int tmp=frameCount;
      score =  tmp - score;
      println("ok!");
      itmp++;
    }
    textSize(50);
    textAlign(CENTER);
    fill(255);
    
    text("Animate life became extinct!",width/2,height/2);
    text("Score "+score,width/2,height/2+45);
     
  }

  if ((keyPressed == true) && (key == 's')) {
    start_flag=false;//false(偽)にするとタイトル画面になる
    initialize=true;
    for (int i = 0; i < r.size(); ++i) {
      r.remove(i);
      red_num--;
    }
    for (int i = 0; i < g.size(); ++i) {
      g.remove(i);
      green_num--;
    }
  }
  if ((keyPressed == true) && (key == '1')) {
    pattern=1;
  }
  if ((keyPressed == true) && (key == '2')) {
    pattern=2;
  }
  
}
//子孫繁栄関数
void bear(){

  //赤の繁栄判定
  int x = detect_collision('r');
  int y = detect_collision('g');
  
  if( x != 0 && red_num <team_red-1){
    r.add(new Red(r.get(x).xPos,r.get(x).yPos,random(10000)/10000.f * (PI*2),1+random(2),r.get(x).generation+ 1 ));
    red_num++; 
  }
  if( y != 0 && green_num <team_green-1){
    g.add(new Green(g.get(y).xPos,g.get(y).yPos,random(10000)/10000.f * (PI*2),1+random(2),g.get(y).generation+ 1 ));
    green_num++;
    g.add(new Green(g.get(y).xPos,g.get(y).yPos,random(10000)/10000.f * (PI*2),1+random(2),g.get(y).generation+ 1 ));
    green_num++; 
   
  }

  
  
}
//当たり判定
int detect_collision(char team_color){

  if(team_color == 'r'){
    for(int i = 0;i < red_num;i++){
      for(int j = i+1;j < red_num;j++){
            
        if(i!=j && pow(r.get(i).xPos-r.get(j).xPos,2) + pow(r.get(i).yPos-r.get(j).yPos,2) < 64 ) {
          
          if(r.get(i).generation < r.get(j).generation){
            return j;
          }
       
          else{
            return i;
          }  
        }
      }
    }
  }  
  if(team_color == 'g'){
    for(int i = 0;i < green_num;i++){
      for(int j = i+1;j < green_num;j++){
            
        if(i!=j && pow(g.get(i).xPos-g.get(j).xPos,2) + pow(g.get(i).yPos-g.get(j).yPos,2) < 100 ) {
          
          if(g.get(i).generation < g.get(j).generation){
            return j;
          }
       
          else{
            return i;
          }  
        }
      }
    }
  }  

  
  else{
    return 0;
  }
  

  return 0;
}

//捕食
void eat(char team_color){
  if(team_color == 'r'){
    for(int i = 0;i < red_num-1;i++){
      for(int j = 0;j < green_num-1;j++){
        if(pow(r.get(i).xPos-g.get(j).xPos,2) + pow(r.get(i).yPos-g.get(j).yPos,2) < 36 ) {
          g.remove(j);
          green_num--;
          r.get(i).hungry+=100;  
        }
      }
    }
  } 
 /* 
  if(team_color == 'g'){
    for(int i = 0;i < green_num-1;i++){
      for(int j = 0;j < blue_num-1;j++){
        if(pow(g.get(i).xPos-b.get(j).xPos,2) + pow(g.get(i).yPos-b.get(j).yPos,2) < 36 ) {
          b.remove(j);
          blue_num--;
          r.get(i).hungry+=100;  
        }
      }
    }
  }  */
}
//赤チームクラス
public class Red{

  color red;//色情報の定義(まだ赤色は入ってない)
  
  float xPos,yPos,rad,speed;//x座標、ｙ座標、角度（ラジアン）、移動スピード
  int generation,hungry;//世代、空腹度
  boolean live;//生死状態　trueなら生　falseなら死
  //static int red_num=5;
  
  Red(float x,float y,float r,float s,int g){//olor c){
    xPos = x;
    yPos = y;
    rad = r;
    speed = s;
    generation = g;
    hungry = 1000;
    live = true;
    red = color(255,0,0);

  }

  //描画
  void display(){
      if(hungry!=0){
        fill(red);
        textSize(20);
        text(generation,xPos,yPos);
      }
  }
  //移動
  void move(){
    if(xPos > 0 && yPos > 0 && xPos < width&& yPos < height){
      if (pattern==2) {
        if(frameCount % 15 == 0){
          int choise = int(random(3));
          
          if (choise==0) {
            rad+=random(2);
          }
          if (choise==1) {
            rad+=random(4);
          }
          if (choise==2) {
            rad-=random(2);
          }
          if (choise==3) {
            rad-=random(4);
          }
        }
        xPos += cos(rad) * speed;
        yPos += sin(rad) * speed;
      }
      if(pattern==1){
        xPos += cos(rad) * speed;
        yPos += sin(rad) * speed;
      }
    }
    if(yPos < 15 || yPos > height - 5){
      rad = 2*PI - rad;//上下の壁を超えそうになったら反射
    }
    if(xPos < 5 || xPos > width - 5){
      rad = PI - rad;//左右の壁を超えそうになったら反射
    }
    
    if(second()/2 == 0){
      hungry=hungry-1;   
    }
  }
  void die_of_hunger(int i){
    r.remove(i);     
  }
}

public class Green{

  color green;//色情報の定義(まだ赤色は入ってない)
  
  float xPos,yPos,rad,speed;//x座標、ｙ座標、角度（ラジアン）、移動スピード
  int generation,hungry;//世代、空腹度
  boolean live;//生死状態　trueなら生　falseなら死
  
  Green(float x,float y,float r,float s,int g){//olor c){
    xPos = x;
    yPos = y;
    rad = r;
    speed = s;
    generation = g;
    hungry = 300;
    live = true;
    green = color(0,255,0);

  }

  //描画
  void display(){
      if(hungry!=0){
        fill(green);
        textSize(20);
        text(generation,xPos,yPos);
      }
  }
  //移動
  void move(){
    if(xPos > 0 && yPos > 0 && xPos < width&& yPos < height){
      if (pattern==2) {
        if(frameCount % 15 == 0){
          int choise = int(random(3));
          
          if (choise==0) {
            rad+=random(2);
          }
          if (choise==1) {
            rad+=random(4);
          }
          if (choise==2) {
            rad-=random(2);
          }
          if (choise==3) {
            rad-=random(4);
          }
        }
        xPos += cos(rad) * speed;
        yPos += sin(rad) * speed;
      }
      if(pattern==1){
        xPos += cos(rad) * speed;
        yPos += sin(rad) * speed;
      }
      
      if(yPos < 15 || yPos > height - 5){
        rad = 2*PI - rad;//上下の壁を超えそうになったら反射
      }
      if(xPos < 5 || xPos > width - 5){
        rad = PI - rad;//左右の壁を超えそうになったら反射
      }
      if(second()/2 == 0){
        hungry=hungry-1;   
      }
    }
  }
  void die_of_hunger(int i){
    g.remove(i);   
  }
}


